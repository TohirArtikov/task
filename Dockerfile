FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/SavvyEnglish-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} savvy-english-api.jar
ENTRYPOINT ["java","-jar","/savvy-english-api.jar"]