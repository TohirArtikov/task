package AppDevelopment.Exam;

import AppDevelopment.Exam.entity.PaymentEntity;
import AppDevelopment.Exam.entity.user.UserEntity;
import AppDevelopment.Exam.repository.PaymentRepository;
import AppDevelopment.Exam.repository.UserRepository;
import AppDevelopment.Exam.util.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Arrays;

@Component
@Slf4j
public class DataInitializer implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (userRepository.count() == 0){
            userRepository.save(UserEntity.builder()
                .username("tahir")
                .password(this.passwordEncoder.encode("123456"))
                .roles(Arrays.asList( "ADMIN"))
                .email("tahir_artikov@mail.ru")
                .phoneNumber("+998943148990")
                .build()
            );
            paymentRepository.save(PaymentEntity.builder()
                    .cash(new BigDecimal(Double.toString(8.00)))
                    .userId(1L)
                    .build()
            );


        }

    }
}
