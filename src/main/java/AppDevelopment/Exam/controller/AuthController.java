package AppDevelopment.Exam.controller;

import AppDevelopment.Exam.dto.AuthenticationRequest;
import AppDevelopment.Exam.dto.CreateUserDto;
import AppDevelopment.Exam.entity.PaymentEntity;
import AppDevelopment.Exam.entity.user.LoginAttemptEntity;
import AppDevelopment.Exam.entity.user.TokenEntity;
import AppDevelopment.Exam.entity.user.UserEntity;
import AppDevelopment.Exam.repository.LoginAttemptRepository;
import AppDevelopment.Exam.repository.PaymentRepository;
import AppDevelopment.Exam.repository.TokenRepository;
import AppDevelopment.Exam.repository.UserRepository;
import AppDevelopment.Exam.security.jwt.JwtTokenProvider;
import AppDevelopment.Exam.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/api")
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PaymentRepository paymentRepository;

    @Autowired
    TokenRepository tokenRepository;

    @Autowired
    LoginAttemptRepository loginAttemptRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${security.jwt.token.expire-length}")
    private long validityInMilliseconds; // 1h

    @PostMapping("/login")
    public ResponseEntity<?> login(HttpServletRequest request, @RequestBody AuthenticationRequest data) {
        String origin = request.getHeader("Origin");

            String username = data.getUsername();
            UserEntity userEntity = this.userRepository.findByUsername(username)
                    .orElseThrow(() -> new UsernameNotFoundException("Username " + username + "not found"));
            String role = userEntity.getRoles().iterator().next();
            Date now = new Date();
            Date validity = new Date(now.getTime() + validityInMilliseconds);
            Optional<UserEntity> userEntityOptional = userRepository.findByUsername(data.getUsername());
            if (userEntityOptional.isPresent()){
                Optional<LoginAttemptEntity> loginAttemptEntityOptional = loginAttemptRepository.findByUsername(userEntityOptional.get().getUsername());
                if (loginAttemptEntityOptional.isPresent()){
                    if(passwordEncoder.matches(data.getPassword(),userEntityOptional.get().getPassword())){
                        if (loginAttemptEntityOptional.get().getExpireDate() != null)
                            if(!loginAttemptEntityOptional.get().getExpireDate().before(new Date()))
                                return ResponseEntity.ok("user blocked");

                        loginAttemptEntityOptional.get().setAttempt(0L);
                        loginAttemptEntityOptional.get().setExpireDate(null);
                        loginAttemptRepository.save(loginAttemptEntityOptional.get());
                    }else {
                        if (loginAttemptEntityOptional.get().getAttempt()+1L >= 7L)
                            loginAttemptEntityOptional.get().setExpireDate(validity);
                        loginAttemptEntityOptional.get().setAttempt(loginAttemptEntityOptional.get().getAttempt()+1);
                        loginAttemptRepository.save(loginAttemptEntityOptional.get());
                            return ResponseEntity.ok("password is incorrect");
                    }
                }else
                {
                    LoginAttemptEntity loginAttemptEntity = new LoginAttemptEntity();
                    if(passwordEncoder.matches(data.getPassword(),userEntityOptional.get().getPassword())){
                        loginAttemptEntity.setAttempt(0L);
                        loginAttemptEntity.setUsername(data.getUsername());
                        loginAttemptRepository.save(loginAttemptEntity);
                    }
                    else{
                        loginAttemptEntity.setAttempt(1L);
                        loginAttemptEntity.setUsername(data.getUsername());
                        loginAttemptRepository.save(loginAttemptEntity);
                        return ResponseEntity.ok("password is incorrect");
                    }

                }
            }else {
                return ResponseEntity.ok("such a user does not exist");
            }


            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));
            String token = jwtTokenProvider.createToken(username, userEntity.getRoles());

            userRepository.save(userEntity);
            Map<Object, Object> userData = new HashMap<>();
            userData.put("username", username);
            userData.put("userId", userEntity.getId());
            userData.put("role", role);

            Map<Object, Object> model = new HashMap<>();
            model.put("userData", userData);
            model.put("token", token);
            //save token
            TokenEntity tokenEntity = new TokenEntity();
            tokenEntity.setTokenId(token);
            tokenEntity.setUserId(userEntity.getId());
            tokenEntity.setName(username);
            Optional<TokenEntity> optional = tokenRepository.findByUserId(userEntity.getId());
            if (optional.isPresent()){
                optional.get().setTokenId(token);
                optional.get().setDeleted(false);
                tokenRepository.save(optional.get());
            }else {
                tokenRepository.save(tokenEntity);
            }

            return ResponseEntity.ok(model);
    }

    @PostMapping("/signup")
    public ResponseEntity registerUser(@Valid @RequestBody CreateUserDto signUpRequest) {
        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.ok("Email address already in use.");
        }
        if(userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.ok("user name already in use.");
        }
        if(userRepository.existsByPhoneNumber(signUpRequest.getPhonenumber())) {
            return ResponseEntity.ok("Phonenumber already in use.");
        }

        // Creating user's account
        UserEntity user = new UserEntity();
        user.setUsername(signUpRequest.getUsername());
        user.setEmail(signUpRequest.getEmail());
        user.setPhoneNumber(signUpRequest.getPhonenumber());
        user.setRoles(Arrays.asList( "User"));
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

        userRepository.save(user);
        PaymentEntity paymentEntity = new PaymentEntity();
        paymentEntity.setUserId(user.getId());
        paymentRepository.save(paymentEntity);
        return ResponseEntity.ok("Successfully saved!");

    }

    @GetMapping("/logout")
    public ResponseEntity<?> logOut(){
        Long id = SecurityUtils.getUserId();
        Optional<TokenEntity> optional = tokenRepository.findByUserId(id);
        if (optional.isPresent()){
            optional.get().setDeleted(true);
            tokenRepository.save(optional.get());
            return ResponseEntity.ok("Successfully Log Out");
        }
        return ResponseEntity.ok("No information found");
    }





}
