package AppDevelopment.Exam.controller;

import AppDevelopment.Exam.dto.PaymentDto;
import AppDevelopment.Exam.entity.PaymentEntity;
import AppDevelopment.Exam.repository.PaymentRepository;
import AppDevelopment.Exam.util.SecurityUtils;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.Option;
import java.math.BigDecimal;
import java.util.Optional;

@RestController
@RequestMapping("/api/payment")
public class PaymentController {
    @Autowired
    PaymentRepository paymentRepository;

    @GetMapping
    public ResponseEntity<?> payment(){
        Long id = SecurityUtils.getUserId();
        Optional<PaymentEntity> optional = paymentRepository.findByUserId(id);
        PaymentDto paymentDto;
        if (optional.isPresent()){
            if (optional.get().getCash().compareTo(new BigDecimal(Double.toString(1.1))) >= 0){
                optional.get().setCash(optional.get().getCash().subtract(new BigDecimal(Double.toString(1.1))));
                paymentRepository.save(optional.get());
                paymentDto = new PaymentDto(id,optional.get().getCash());
            }else{
                return ResponseEntity.ok("Funds are not enough!");
            }
            return ResponseEntity.ok(paymentDto);
        }
        return ResponseEntity.ok("No information found");

    }

}
