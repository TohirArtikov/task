package AppDevelopment.Exam.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@NoArgsConstructor
public class PaymentDto {
    private Long id;
    private BigDecimal cash;

    public PaymentDto(Long id, BigDecimal cash) {
        this.cash = cash;
        this.id = id;

    }
}
