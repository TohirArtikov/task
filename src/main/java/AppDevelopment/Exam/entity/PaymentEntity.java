package AppDevelopment.Exam.entity;

import AppDevelopment.Exam.entity.base.AbstractAuditableEntity;
import AppDevelopment.Exam.entity.user.UserEntity;
import AppDevelopment.Exam.util.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
@Entity
@Table(name="payments")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentEntity extends AbstractAuditableEntity<UserEntity, Long>{
    private Long userId;
    private BigDecimal cash = new BigDecimal(Double.toString(8.00));
}
