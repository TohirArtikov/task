package AppDevelopment.Exam.entity.user;

import AppDevelopment.Exam.entity.base.AbstractAuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="attempts")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginAttemptEntity extends AbstractAuditableEntity<UserEntity, Long> {
    private String username;
    private Date expireDate = null;
    private Long attempt = 0L;
}
