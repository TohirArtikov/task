package AppDevelopment.Exam.entity.user;

import AppDevelopment.Exam.entity.base.AbstractAuditableEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="tokens")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TokenEntity extends AbstractAuditableEntity<UserEntity, Long> {
    private String tokenId;
    private String name;
    private Long userId;
    private Boolean deleted = false;
}
