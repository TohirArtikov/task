package AppDevelopment.Exam.repository;

import AppDevelopment.Exam.entity.PaymentEntity;
import AppDevelopment.Exam.entity.user.LoginAttemptEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LoginAttemptRepository extends JpaRepository<LoginAttemptEntity, Long>{
    Optional<LoginAttemptEntity> findByUsername(String username);
}
