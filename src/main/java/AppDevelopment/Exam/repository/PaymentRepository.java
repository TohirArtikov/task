package AppDevelopment.Exam.repository;

import AppDevelopment.Exam.entity.PaymentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PaymentRepository extends JpaRepository<PaymentEntity, Long>{
    Optional<PaymentEntity> findByUserId(Long id);
}
