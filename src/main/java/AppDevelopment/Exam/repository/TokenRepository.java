package AppDevelopment.Exam.repository;

import AppDevelopment.Exam.entity.PaymentEntity;
import AppDevelopment.Exam.entity.user.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TokenRepository extends JpaRepository<TokenEntity, Long>{



    Optional<TokenEntity> findByUserId(Long id);

    Optional<TokenEntity> findByTokenId(String token);
}
