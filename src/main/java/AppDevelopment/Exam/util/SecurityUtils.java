package AppDevelopment.Exam.util;

import AppDevelopment.Exam.entity.user.UserEntity;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static Long getUserId() {
        UserEntity user = getUserDetails();
        if (user != null) {
            return user.getId();
        }
        return null;
    }

    public static UserEntity getUserDetails() {
        AbstractAuthenticationToken authentication = (AbstractAuthenticationToken)
                SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal() instanceof UserEntity) {
            return (UserEntity) authentication.getPrincipal();
        }
        return null;
    }


}
